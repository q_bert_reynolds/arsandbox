﻿using UnityEngine;
using System.Collections;
using Windows.Kinect;

public class DepthSourceView : MonoBehaviour
{
    public int nearPlane = 1000;
    public int farPlane = 1400;
    public float depthScale = 0.05f;
    [Range(0,1)] public float normalSmoothing = 0.5f;
    [Range(1,60)] public int collisionUpdateFrameInterval = 10;
    
    public GameObject ColorSourceManager;
    public GameObject DepthSourceManager;
    
    private KinectSensor _Sensor;
    private CoordinateMapper _Mapper;
    private Mesh _Mesh;
    private Mesh collisionMesh;
    private Vector3[] _Vertices;
    private Vector2[] _UV;
    private int[] _Triangles;
    private int frameCounter = 0;
    
    // Only works at 4 right now
    private const int _DownsampleSize = 4;
    private const int _Speed = 50;
    
    private ColorSourceManager _ColorManager;
    private DepthSourceManager _DepthManager;

    private Texture2D tex;
    private Color[] colors;

    void Start() {
        _Sensor = KinectSensor.GetDefault();
        if (_Sensor != null) {
            _Mapper = _Sensor.CoordinateMapper;
            var frameDesc = _Sensor.DepthFrameSource.FrameDescription;
            tex = new Texture2D(480, 270, TextureFormat.R8, false);
            colors = new Color[480*270];

            CreateMesh(frameDesc.Width / _DownsampleSize, frameDesc.Height / _DownsampleSize);

            if (!_Sensor.IsOpen) _Sensor.Open();
        }
    }

    Vector3[] oldNormals;
    Vector3[] newNormals;
    void CreateMesh(int width, int height) {
        _Mesh = new Mesh();
        collisionMesh = new Mesh();
        GetComponent<MeshFilter>().sharedMesh = _Mesh;

        _Vertices = new Vector3[width * height];
        _UV = new Vector2[width * height];
        _Triangles = new int[6 * ((width - 1) * (height - 1))];

        int triangleIndex = 0;
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int index = (y * width) + x;

                _Vertices[index] = new Vector3(width/2-x, height/2-y, 0);
                _UV[index] = new Vector2((1-(float)x / (float)width), ((float)y / (float)height));

                // Skip the last row/col
                if (x != (width - 1) && y != (height - 1)) {
                    int topLeft = index;
                    int topRight = topLeft + 1;
                    int bottomLeft = topLeft + width;
                    int bottomRight = bottomLeft + 1;

                    _Triangles[triangleIndex++] = topLeft;
                    _Triangles[triangleIndex++] = bottomLeft;
                    _Triangles[triangleIndex++] = topRight;
                    _Triangles[triangleIndex++] = bottomLeft;
                    _Triangles[triangleIndex++] = bottomRight;
                    _Triangles[triangleIndex++] = topRight;
                }
            }
        }

        _Mesh.MarkDynamic();
        _Mesh.vertices = _Vertices;
        _Mesh.uv = _UV;
        _Mesh.triangles = _Triangles;
        _Mesh.RecalculateNormals();
        collisionMesh.vertices = _Vertices;
        collisionMesh.triangles = _Triangles;
        collisionMesh.RecalculateNormals();
        oldNormals = _Mesh.normals;
        newNormals = _Mesh.normals;
    }

    void Update() {
        if (_Sensor == null) return;

        if (ColorSourceManager == null) return;
        
        _ColorManager = ColorSourceManager.GetComponent<ColorSourceManager>();
        if (_ColorManager == null) return;
        
        if (DepthSourceManager == null) return;
        
        _DepthManager = DepthSourceManager.GetComponent<DepthSourceManager>();
        if (_DepthManager == null) return;

        
        gameObject.GetComponent<Renderer>().sharedMaterial.SetTexture("_DepthTex", tex);
        gameObject.GetComponent<Renderer>().sharedMaterial.mainTexture = _ColorManager.GetColorTexture();
        RefreshData(_DepthManager.GetData(),
            _ColorManager.ColorWidth,
            _ColorManager.ColorHeight);
    }

    public static Vector3[] highPoints;
    public static Vector3[] lowPoints;
    public float peakSpacing = 15;
    private void RefreshData(ushort[] depthData, int colorWidth, int colorHeight)
    {
        Windows.Kinect.FrameDescription frameDesc = _Sensor.DepthFrameSource.FrameDescription;
        int width = frameDesc.Width;
        int height = frameDesc.Height;
        int pixelCount = depthData.Length;
        ColorSpacePoint[] colorSpace = new ColorSpacePoint[pixelCount];
        _Mapper.MapDepthFrameToColorSpace(depthData, colorSpace);
        
        for (int y = 0; y < height; y += _DownsampleSize)
        {
            for (int x = 0; x < width; x += _DownsampleSize)
            {
                int indexX = x / _DownsampleSize;
                int indexY = y / _DownsampleSize;
                int smallIndex = (indexY * (width / _DownsampleSize)) + indexX;
                
                double avg = GetAvg(depthData, x, y, width, height);
                
                avg = avg * depthScale;
                
                _Vertices[smallIndex].z = (float)avg;
                
                // Update UV mapping with CDRP
                var colorSpacePoint = colorSpace[(y * width) + x];
                _UV[smallIndex] = new Vector2(colorSpacePoint.X / colorWidth, colorSpacePoint.Y / colorHeight);
            }
        }

        DepthSpacePoint[] depthSpace = new DepthSpacePoint[colorWidth*colorHeight];
        _Mapper.MapColorFrameToDepthSpace(depthData, depthSpace);
        for (int y = 0; y < 270; y++)
        {
            for (int x = 0; x < 480; x++)
            {
                DepthSpacePoint p = depthSpace[y * 4 * colorWidth + x * 4];
                int pX = (int)p.X;
                int pY = (int)p.Y;
                if (pY < 0 || pY >= height || pX < 0 || pX >= width) continue;
                ushort d = depthData[pY * width + pX];
                float c = (float)(d - nearPlane) / (float)(farPlane - nearPlane);
                colors[y * 480 + x] = new Color(c,c,c,1);
            }
        }
        tex.SetPixels(colors);
        tex.Apply();
        
        _Mesh.vertices = _Vertices;
        _Mesh.uv = _UV;
        _Mesh.triangles = _Triangles;
        for (int i = 0; i < oldNormals.Length; i++) oldNormals[i] = newNormals[i];
        _Mesh.RecalculateNormals();
        newNormals = _Mesh.normals;
        float t = Mathf.Lerp(1f, Time.unscaledDeltaTime * Time.unscaledDeltaTime * 0.0001f, normalSmoothing);
        for (int i = 0; i < oldNormals.Length; i++) 
        {
            newNormals[i] = Vector3.Lerp(oldNormals[i], newNormals[i], t);
        }
        _Mesh.normals = newNormals;
        _Mesh.RecalculateBounds();

        if (frameCounter % collisionUpdateFrameInterval == 0)
        {
            collisionMesh.vertices = _Vertices;
            collisionMesh.RecalculateNormals();
            collisionMesh.RecalculateBounds();
            GetComponent<MeshCollider>().sharedMesh = collisionMesh;

            Vector3 diff = ReprojectionManager.worldBounds.size;
            int columns = Mathf.CeilToInt(diff.x / peakSpacing);
            int rows = Mathf.CeilToInt(diff.z / peakSpacing);
            int peaks = columns * rows;
            highPoints = new Vector3[peaks];
            lowPoints = new Vector3[peaks];
            for (int i = 0; i < peaks; i++) {
                highPoints[i] = Vector3.down * float.MaxValue;
                lowPoints[i] = Vector3.up * float.MaxValue;
            }

            float cutoff = 4+nearPlane/16;
            Vector3 corner = ReprojectionManager.worldBounds.min;
            for (int i = 0; i < _Vertices.Length; i++) {
                if (_Vertices[i].z > cutoff) continue;// reject peaks that are too high

                Vector3 p = transform.TransformPoint(_Vertices[i]);
                if (!ReprojectionManager.worldBounds.Contains(p)) continue;// reject peaks that are out of bounds
                
                diff = p - corner;
                int row = Mathf.FloorToInt(diff.z/peakSpacing);
                int column = Mathf.FloorToInt(diff.x/peakSpacing);
                int index = column*rows+row;
                if (index < 0 || index >= highPoints.Length) continue;// reject peaks whose indices are out of range
                
                bool tooClose = false;
                for (int j = 0; j < index; j++) {
                    Vector3 otherPeak = highPoints[j];
                    diff = p - otherPeak;
                    diff.y = 0;
                    if (diff.sqrMagnitude < 16) {
                        tooClose = true;
                        break;
                    }
                }
                if (tooClose) continue;// reject peaks that are too close to other peaks

                if (p.y > highPoints[index].y) highPoints[index] = p;
                else if (p.y < lowPoints[index].y) lowPoints[index] = p;
            }
        }
        frameCounter++;
    }
    
    private double GetAvg(ushort[] depthData, int x, int y, int width, int height)
    {
        double sum = 0.0;
        
        for (int y1 = y; y1 < y + 4; y1++)
        {
            for (int x1 = x; x1 < x + 4; x1++)
            {
                int fullIndex = (y1 * width) + x1;
                
                ushort d = depthData[fullIndex];
                if (d == 0 || d > 6000) sum += 6000;
                else sum += depthData[fullIndex];
                
            }
        }

        return sum / 16;
    }

    void OnApplicationQuit()
    {
        if (_Mapper != null)
        {
            _Mapper = null;
        }
        
        if (_Sensor != null)
        {
            if (_Sensor.IsOpen)
            {
                _Sensor.Close();
            }

            _Sensor = null;
        }
    }

    void OnDrawGizmos () {
        if (highPoints == null) return;
        for (int i = 0; i < highPoints.Length; i++) {
            Gizmos.color = Color.black;
            Gizmos.DrawWireSphere(highPoints[i], 2);
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(lowPoints[i], 2);
        }
    }
}
