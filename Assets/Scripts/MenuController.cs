using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{

    public GameObject bg;
    public GameObject menuRoot;
    public Text prompt;

    public const float unitsToFeet = 0.0667f;

    public bool showDepthVisualization = true;

    public Slider maxDepthSlider;
    public Slider minDepthSlider;
    public Slider fovSlider;
    public Button depthVisualizationToggle;

    Text maxDepthText;
    Text minDepthText;
    Text fovText;
    Text depthVisualizationText;

    public enum Status {
        Game,
        Menu,
        SettingWorldCorners,
        SettingMaskCorners
    }
    public Status status = Status.Game;

    bool maskSet = false;
    bool worldSet = false;
    bool canRaycast = false;

    Vector3[] prevWorldCorners = new Vector3[4];
    Vector2[] prevMaskCorners = new Vector2[4];
    Vector3[] corners = new Vector3[4];
    int index = 0;

    string[] cornerPromptTexts = new string [] {
        "Click the lower left corner",
        "Click the upper left corner",
        "Click the upper right corner",
        "Click the lower right corner"
    };

    string maskPromptText = " of the desired projection area (in real life).";
    string worldPromptText = " of the sandbox (on the camera feed).";

    void Awake () {
        prompt.enabled = false;
        menuRoot.SetActive(false);
        bg.SetActive(false);

        maxDepthText = maxDepthSlider.GetComponentInChildren<Text>();
        minDepthText = minDepthSlider.GetComponentInChildren<Text>();
        fovText = fovSlider.GetComponentInChildren<Text>();
        depthVisualizationText = depthVisualizationToggle.GetComponentInChildren<Text>();
    }

    void Start () {
        maxDepthSlider.value = ReprojectionManager.settings.maxDepth;
        minDepthSlider.value = ReprojectionManager.settings.minDepth;
        fovSlider.value = ReprojectionManager.settings.fov;
        UpdateText();
        Cursor.visible = false;
    }

    void Update () {
        if (Input.GetKeyDown(KeyCode.Escape)) CancelPressed();

        if (!Input.GetMouseButtonDown(0) || !canRaycast) return;
        if (status == Status.SettingMaskCorners) SetMaskCorner();
        else if (status == Status.SettingWorldCorners) SetWorldCorner();
    }

    void CancelPressed () {
        switch (status) {
            case Status.Game:
                ShowMenu();
                break;
            case Status.Menu:
                ReturnToGame();
                break;
            case Status.SettingMaskCorners:
                ReprojectionManager.SetMaskCorners(prevMaskCorners);
                ShowMenu();
                break;
            case Status.SettingWorldCorners:
                ReprojectionManager.SetMaskCorners(prevMaskCorners);
                ReprojectionManager.SetWorldCorners(prevWorldCorners);
                ShowMenu();
                break;
        }
    }

    void UpdateText () {
        float scale = unitsToFeet * ReprojectionManager.depthSourceView.depthScale;
        maxDepthText.text = string.Format("Max Depth - {0:0.00}ft", ReprojectionManager.settings.maxDepth * scale);
        minDepthText.text = string.Format("Min Depth - {0:0.00}ft", ReprojectionManager.settings.minDepth * scale);
        fovText.text = string.Format("Field of View - {0:0.0#}°", ReprojectionManager.settings.fov);
        depthVisualizationText.text = showDepthVisualization ? "Hide Depth" : "Show Depth";
    }

    void SetMaskCorner () {
        Vector3 corner = Input.mousePosition;
        corner.x /= Screen.width;
        corner.y /= Screen.height;
        corners[index] = corner;
        ReprojectionManager.SetMaskCorners(System.Array.ConvertAll<Vector3, Vector2>(corners, c => (Vector2)c));

        index++;
        if (index == 4) {
            maskSet = true;
            if (worldSet) {
                ReprojectionManager.instance.RecalculateTransform();
                ReprojectionManager.SaveSettings();
            }
            ShowMenu();
            return;
        }

        prompt.text = cornerPromptTexts[index] + maskPromptText;
    }

    void SetWorldCorner () {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (!ReprojectionManager.depthMeshCollider.Raycast(ray, out hit, 1000)) return;

        corners[index] = hit.point;

        index++;
        if (index == 4) {
            worldSet = true;
            ReprojectionManager.SetMaskCorners(prevMaskCorners);
            ReprojectionManager.SetWorldCorners(corners);
            if (maskSet) {
                ReprojectionManager.instance.RecalculateTransform();
                ReprojectionManager.SaveSettings();
            }
            ShowMenu();
            return;
        }

        prompt.text = cornerPromptTexts[index] + worldPromptText;
    }

    public void SaveSettings () {
        ReprojectionManager.SaveSettings();
    }

#region UI Buttons
    void ShowMenu () {
        Cursor.visible = true;
        if (showDepthVisualization) ReprojectionManager.ShowDebugColors();
        else ReprojectionManager.ShowGameMaterial();
        menuRoot.SetActive(true);
        bg.SetActive(true);
        prompt.enabled = false;
        Time.timeScale = 0;
        status = Status.Menu;
    }

    void ReturnToGame () {
        Cursor.visible = false;
        ReprojectionManager.ShowGameMaterial();
        menuRoot.SetActive(false);
        bg.SetActive(false);
        Time.timeScale = 1;
        status = Status.Game;
    }

    IEnumerator DelayRaycast () {
        yield return new WaitForEndOfFrame();
        while (Input.GetMouseButton(0)) yield return null;
        canRaycast = true;
    }

    public void SetWorldCorners () {
        canRaycast = false;
        menuRoot.SetActive(false);
        bg.SetActive(false);
        prompt.enabled = true;
        prompt.text = cornerPromptTexts[0] + worldPromptText;
        status = Status.SettingWorldCorners;
        index = 0;
        prevMaskCorners = ReprojectionManager.instance.maskCorners;
        ClearMask();
        prevWorldCorners = ReprojectionManager.instance.worldCorners;
        ReprojectionManager.ShowCameraFeed();
        StopAllCoroutines();
        StartCoroutine(DelayRaycast());
    }

    public void ResetReprojection () {
        maskSet = false;
        worldSet = false;
        ReprojectionManager.ResetReprojection();
        UpdateText();
    }

    public void SetMaskCorners () {
        canRaycast = false;
        menuRoot.SetActive(false);
        bg.SetActive(false);
        prompt.enabled = true;
        prompt.text = cornerPromptTexts[0] + maskPromptText;
        status = Status.SettingMaskCorners;
        index = 0;
        prevMaskCorners = ReprojectionManager.instance.maskCorners;
        ClearMask();
        corners = new Vector3[] { Vector2.zero, Vector2.up, Vector2.one, Vector2.right };
        ReprojectionManager.ShowCameraFeed();
        StopAllCoroutines();
        StartCoroutine(DelayRaycast());
    }

    public void ShowHideDepthVisualization () {
        showDepthVisualization = !showDepthVisualization;
        if (showDepthVisualization) ReprojectionManager.ShowDebugColors();
        else ReprojectionManager.ShowGameMaterial();
        UpdateText();
    }
#endregion

#region UI Sliders
    public void ClearMask () {
        ReprojectionManager.SetMaskCorners(new Vector2[] {Vector2.zero, Vector2.up, Vector2.one, Vector2.right});
    }

    public void ResetGame () {
        ApplicationManager.RestartGame();
    }

    public void SetFieldOfView (float fov) {
        ReprojectionManager.cam.fieldOfView = fov;
        ReprojectionManager.settings.fov = fov;
        UpdateText();
    }

    public void SetHorizontalOffset (float xOffset) {
        
    }

    public void SetVerticalOffset (float yOffset) {
        
    }

    public void SetMaxDepth (float depth) {
        ReprojectionManager.depthSourceView.farPlane = (int)depth;
        ReprojectionManager.settings.maxDepth = (int)depth;
        UpdateText();
    }

    public void SetMinDepth (float depth) {
        ReprojectionManager.depthSourceView.nearPlane = (int)depth;
        ReprojectionManager.settings.minDepth = (int)depth;
        UpdateText();
    }
#endregion
}
