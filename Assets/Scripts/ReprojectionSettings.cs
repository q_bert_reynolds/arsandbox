using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ReprojectionSettings {

    public Vector3[] worldCorners;
    public Vector2[] maskCorners;
    public int minDepth;
    public int maxDepth;
    public float fov;
    public Vector3 cameraPosition;
    public Quaternion cameraRotation;
}