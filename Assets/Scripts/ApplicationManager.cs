using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ApplicationManager : MonoBehaviour
{
    public static ApplicationManager instance;

    void Awake () {
        if (instance == null) {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else {
            Destroy(gameObject);
        }
    }
   
    public static void RestartGame () {
        Time.timeScale = 0;
        Destroy(Spawner.instance);
        KidController.kids.Clear();
        KidController.lastDespawnTime = Time.time;
        KidController.spawnCount = 0;
        KidController.activeCount = 0;
        SceneManager.LoadScene("Game");
        Time.timeScale = 1;
    }
}
