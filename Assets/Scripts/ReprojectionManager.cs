using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ReprojectionManager : MonoBehaviour
{
    public static ReprojectionManager instance;

    public Material debugMaterial;
    public Material gameMaterial;

    private static Material _mask;
    private static Material mask {
        get {
            if (_mask == null) {
                _mask = new Material(Shader.Find("Snow Kids/Black"));
                _mask.hideFlags = HideFlags.HideAndDontSave;
            }        
            return _mask;
        }
    }

    public static Bounds worldBounds;
    [Tooltip("BottomLeft, TopLeft, TopRight, BottomRight")] public Vector3[] worldCorners = new Vector3[4];
    [Tooltip("BottomLeft, TopLeft, TopRight, BottomRight")] public Vector2[] maskCorners = new Vector2[] {
        Vector2.zero, Vector2.up, Vector2.one, Vector2.right
    };
    
    public static ReprojectionSettings settings { get; private set; }
    public static DepthSourceView depthSourceView { get; private set; }
    public static MeshCollider depthMeshCollider { get; private set; }
    public static Camera cam { get; private set; }

    BoxCollider[] walls;
    Vector3 initialPosition;
    Quaternion initialRotation;
    float initialFOV;
    void Awake () {
        instance = this;
        cam = GetComponent<Camera>();
        initialPosition = transform.position;
        initialRotation = transform.rotation;
        initialFOV = cam.fieldOfView;
        
        depthSourceView = FindObjectOfType<DepthSourceView>();
        depthMeshCollider = depthSourceView.GetComponent<MeshCollider>();
        ReprojectionManager.ShowGameMaterial();

        string path = Path.Combine(Application.persistentDataPath, "save.json");
        if (File.Exists(path)) {
            string json = File.ReadAllText(path);
            settings = JsonUtility.FromJson<ReprojectionSettings>(json);
            transform.position = settings.cameraPosition;
            transform.rotation = settings.cameraRotation;
            maskCorners = settings.maskCorners;
            worldCorners = settings.worldCorners;
            depthSourceView.farPlane = settings.maxDepth;
            depthSourceView.nearPlane = settings.minDepth;
            if (settings.fov < 1) settings.fov = cam.fieldOfView;
            else cam.fieldOfView = settings.fov;

        }
        else {
            ClearSettings();
        }

        walls = new BoxCollider[4];
        for (int i = 0; i < 4; i++) {
            GameObject g = new GameObject("wall");
            g.transform.SetParent(transform.parent);
            walls[i] = g.AddComponent<BoxCollider>();
        }
        SetWalls();

        SetWorldBounds();
    }

    public static void ShowDebugColors () {
        depthSourceView.GetComponent<MeshRenderer>().sharedMaterial = instance.debugMaterial;
        instance.debugMaterial.SetFloat("_Blend", 0.6f);
    }

    public static void ShowCameraFeed () {
        depthSourceView.GetComponent<MeshRenderer>().sharedMaterial = instance.debugMaterial;
        instance.debugMaterial.SetFloat("_Blend", 0.05f);
    }

    public static void ShowGameMaterial () {
        depthSourceView.GetComponent<MeshRenderer>().sharedMaterial = instance.gameMaterial;
    }

    public static void SaveSettings () {
        string path = Path.Combine(Application.persistentDataPath, "save.json");
        string json = JsonUtility.ToJson(settings);
        File.WriteAllText(path, json);
    }

    public static void ClearSettings () {
        settings = new ReprojectionSettings();
        settings.cameraPosition = instance.initialPosition;
        settings.cameraRotation = instance.initialRotation;
        settings.maskCorners = new Vector2[] {
            Vector2.zero, Vector2.up, Vector2.one, Vector2.right
        };
        settings.worldCorners = new Vector3[4];
        settings.maxDepth = depthSourceView.farPlane;
        settings.minDepth = depthSourceView.nearPlane;
        settings.fov = cam.fieldOfView;
        SaveSettings();
    }

    public static void ResetReprojection () {
        instance.transform.position = instance.initialPosition;
        instance.transform.rotation = instance.initialRotation;
        cam.fieldOfView = instance.initialFOV;
        ClearSettings();
    }

    public static void SetWorldCorners (Vector3[] corners) {
        if (corners.Length != 4) return;

        settings.worldCorners = corners;
        instance.worldCorners = corners;
        SetWorldBounds();
        instance.SetWalls();
        SaveSettings();
    }

    public static void SetMaskCorners (Vector2[] corners) {
        if (corners.Length != 4) return;

        settings.maskCorners = corners;
        instance.maskCorners = corners;
        SaveSettings();
    }

    public void RecalculateTransform () {
        Vector3 worldCenter = (worldCorners[0] + worldCorners[1] + worldCorners[2] + worldCorners[3]) / 4;
        Vector2 viewportCenter = (maskCorners[0] + maskCorners[1] + maskCorners[2] + maskCorners[3]) / 4;

        float worldLeftLength = (worldCorners[1] - worldCorners[0]).magnitude;
        float worldTopLength = (worldCorners[2] - worldCorners[1]).magnitude;
        float worldRightLength = (worldCorners[3] - worldCorners[2]).magnitude;
        float worldBottomLength = (worldCorners[0] - worldCorners[3]).magnitude;

        Vector3 worldNormal = Vector3.Cross(worldCorners[1] - worldCorners[3], worldCorners[2] - worldCorners[0]);
        transform.rotation = Quaternion.LookRotation(-worldNormal, transform.up);

        Vector3 farClipBottomLeft = cam.ViewportToWorldPoint(new Vector3(maskCorners[0].x, maskCorners[0].y, cam.farClipPlane));
        Vector3 farClipTopLeft = cam.ViewportToWorldPoint(new Vector3(maskCorners[1].x, maskCorners[1].y, cam.farClipPlane));
        Vector3 farClipTopRight = cam.ViewportToWorldPoint(new Vector3(maskCorners[2].x, maskCorners[2].y, cam.farClipPlane));
        Vector3 farClipBottomRight = cam.ViewportToWorldPoint(new Vector3(maskCorners[3].x, maskCorners[3].y, cam.farClipPlane));

        float farClipLeftLength = (farClipTopLeft - farClipBottomLeft).magnitude;
        float farClipTopLength = (farClipTopRight - farClipTopLeft).magnitude;
        float farClipRightLength = (farClipTopRight - farClipBottomRight).magnitude;
        float farClipBottomLength = (farClipBottomRight - farClipBottomLeft).magnitude;

        float leftDist = cam.farClipPlane * worldLeftLength / farClipLeftLength;
        float topDist = cam.farClipPlane * worldTopLength / farClipTopLength;
        float rightDist = cam.farClipPlane * worldRightLength / farClipRightLength;
        float bottomDist = cam.farClipPlane * worldBottomLength / farClipBottomLength;

        float desiredDist = (leftDist + topDist + rightDist + bottomDist) / 4;
        Vector3 desiredWorldCenter = cam.ViewportToWorldPoint(new Vector3(viewportCenter.x, viewportCenter.y, desiredDist));

        transform.position -= desiredWorldCenter - worldCenter;

        settings.cameraPosition = transform.position;
        settings.cameraRotation = transform.rotation;
        SaveSettings();
    }

    static void SetWorldBounds () {
        worldBounds = new Bounds(instance.worldCorners[0], Vector3.zero);
        for (int i = 1; i < 4; i++) worldBounds.Encapsulate(instance.worldCorners[i]);
        Vector3 extents = worldBounds.extents;
        extents.y = 500;
        extents *= 0.9f;//shrink a bit so low and high points aren't off camera
        worldBounds.extents = extents;
    }

    void SetWalls () {
        for (int i = 0; i < 4; i++) {
            BoxCollider wall = walls[i];
            Vector3 a = worldCorners[i];
            Vector3 b = worldCorners[(i+1)%4];
            Vector3 diff = b-a;
            float dist = diff.magnitude;
            Vector3 dir = diff/dist;
            wall.transform.forward = dir;
            wall.transform.position = (a+b)*0.5f - wall.transform.right*5;
            wall.size = new Vector3(10, 50, dist+10);
        }
        gameMaterial.SetVector("_WorldOrigin", worldCorners[0]);
        gameMaterial.SetVector("_WorldSize", worldCorners[2]-worldCorners[0]);
    }

    void OnPostRender () {
        Vector2 bottomLeft = maskCorners[0];
        Vector2 topLeft = maskCorners[1];
        Vector2 topRight = maskCorners[2];
        Vector2 bottomRight = maskCorners[3];

        GL.PushMatrix();
        GL.LoadOrtho();

        mask.SetPass(0);
        GL.Begin(GL.QUADS);

        //bottom left
        GL.Vertex3(0, 0, 0);
        GL.Vertex3(0, bottomLeft.y, 0);
        GL.Vertex3(bottomLeft.x, bottomLeft.y, 0);
        GL.Vertex3(bottomLeft.x, 0, 0);

        //left
        GL.Vertex3(0, bottomLeft.y, 0);
        GL.Vertex3(0, topLeft.y, 0);
        GL.Vertex3(topLeft.x, topLeft.y, 0);
        GL.Vertex3(bottomLeft.x, bottomLeft.y, 0);

        //top left
        GL.Vertex3(0, topLeft.y, 0);
        GL.Vertex3(0, 1, 0);
        GL.Vertex3(topLeft.x, 1, 0);
        GL.Vertex3(topLeft.x, topLeft.y, 0);

        //top
        GL.Vertex3(topLeft.x, topLeft.y, 0);
        GL.Vertex3(topLeft.x, 1, 0);
        GL.Vertex3(topRight.x, 1, 0);
        GL.Vertex3(topRight.x, topRight.y, 0);

        //top right
        GL.Vertex3(topRight.x, topRight.y, 0);
        GL.Vertex3(topRight.x, 1, 0);
        GL.Vertex3(1, 1, 0);
        GL.Vertex3(1, topRight.y, 0);

        //right
        GL.Vertex3(bottomRight.x, bottomRight.y, 0);
        GL.Vertex3(topRight.x, topRight.y, 0);
        GL.Vertex3(1, topRight.y, 0);
        GL.Vertex3(1, bottomRight.y, 0);

        //bottom right
        GL.Vertex3(bottomRight.x, 0, 0);
        GL.Vertex3(bottomRight.x, bottomRight.y, 0);
        GL.Vertex3(1, bottomRight.y, 0);
        GL.Vertex3(1, 0, 0);

        //bottom
        GL.Vertex3(bottomLeft.x, 0, 0);
        GL.Vertex3(bottomLeft.x, bottomLeft.y, 0);
        GL.Vertex3(bottomRight.x, bottomRight.y, 0);
        GL.Vertex3(bottomRight.x, 0, 0);
        
        GL.End();
        GL.PopMatrix();
    }

    // void OnDrawGizmos () {
    //     Gizmos.color = Color.red;
    //     Gizmos.DrawWireCube(worldBounds.center, worldBounds.size);
    // }
}
