using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public int maxKids { 
        get { 
            if (DepthSourceView.highPoints == null || DepthSourceView.highPoints.Length < 2) return 1;
            return DepthSourceView.highPoints.Length - 1; 
        } 
    }
    public float spawnDelay = 3;
    float lastSpawnTime = -1;

    void Update () {
        if (Time.timeScale < 0.1f ||
            KidController.spawnCount >= maxKids || 
            Time.time - KidController.lastDespawnTime < spawnDelay ||
            Time.time - lastSpawnTime < spawnDelay) 
            return;
        
        GameObject kid = Spawner.Spawn("Kid", false);
        kid.SetActive(true);
        lastSpawnTime = Time.time;
    }
}
