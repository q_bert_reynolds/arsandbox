using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Paraphernalia.Components;

[ExecuteInEditMode]
public class KidController : MonoBehaviour
{
    public static List<KidController> kids = new List<KidController>();
    public static int spawnCount = 0;
    public static int activeCount = 0;
    public static float lastDespawnTime = -1;

    public int id = -1;
    public float buryDepth = 0.5f;
    public float sledHeight = 3;

    public enum State {
        Ragdoll,
        Wander,
        Climb,
        ReadySled,
        Sledding,
        Burried,
    }
    public State state = State.Ragdoll;

    public Gradient clothingColors;
    public Gradient skinTones;

    public float walkSpeed = 5;
    public float sledSpeed = 10;
    public float turnSpeed = 30;
    public float maxSpeedChange = 1;

    [Range(1, 5)] public float ragdollIdleTime = 2;
    float lastMovement = -1;

    public Renderer skinRenderer;
    public Renderer jacketRenderer;
    public Renderer scarfRenderer;
    public Renderer hatRenderer;
    public GameObject sled;

    Rigidbody body;
    Animator anim;
    MaterialPropertyBlock propBlock;

    Vector3 highPoint;
    Vector3 lowPoint;
 
    void Awake() {
        body = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
    }

    [ContextMenu("Update Materials")]
    void UpdateMaterials () {
        float r = Random.value;
        propBlock = new MaterialPropertyBlock();
        jacketRenderer.GetPropertyBlock(propBlock);
        propBlock.SetVector("_Color", clothingColors.Evaluate(r));
        jacketRenderer.SetPropertyBlock(propBlock);

        hatRenderer.GetPropertyBlock(propBlock);
        propBlock.SetVector("_Color", clothingColors.Evaluate((r + 0.5f + Random.Range(-0.5f, 0.5f))%1f));
        hatRenderer.SetPropertyBlock(propBlock);

        skinRenderer.GetPropertyBlock(propBlock);
        propBlock.SetVector("_Color", skinTones.Evaluate(Random.value));
        skinRenderer.SetPropertyBlock(propBlock);
    }

    void OnEnable () {
        spawnCount++;
        activeCount++;
        kids.Add(this);
        for (int i = 0; i < kids.Count; i++) {
            kids[i].id = i;
        }
        sled.SetActive(false);
        body.velocity = Vector3.zero;
        UpdateMaterials();
        lastMovement = Time.time;
        state = State.Ragdoll;
        
        UpdateHighLow();
        Vector3 diff = highPoint - lowPoint;
        if (diff.y > sledHeight) { 
            transform.position = highPoint;
            StartSledding();
        }
        else {
            Ray ray = new Ray(ReprojectionManager.worldBounds.center + Vector3.up * 50, Vector3.down);
            RaycastHit hit;
            if (ReprojectionManager.depthMeshCollider != null && ReprojectionManager.depthMeshCollider.Raycast(ray, out hit, 1000)) {
                transform.position = hit.point;
            }
            else {
                transform.position = lowPoint;
            }
            PlayParticle("Pop");
        }
    }

    void OnDisable () {
        kids.Remove(this);
        spawnCount--;
        lastDespawnTime = Time.time;
        if (state != State.Burried) activeCount--;
    }
    
    Vector3 targetVelocity;
    Vector3 climbTarget;
    Vector3 prevPos;
    void Update () {
        if (!Application.isPlaying) return;     

        UpdateHighLow();
        GroundCheck();

        switch (state) {
            case State.Ragdoll:
                UpdateRagdoll();
                AvoidCollisions();
                break;
            case State.Wander:
                UpdateWander();
                AvoidCollisions();
                break;
            case State.Climb:
                UpdateClimb();
                AvoidCollisions();
                break;
            case State.ReadySled:
                UpdateReadySled();
                break;
            case State.Sledding:
                UpdateSledding();
                break;
            case State.Burried:
                UpdateBurried();
                break;
        }

        UpdateOrientation();

        Vector3 diff = prevPos - transform.position;
        if (diff.sqrMagnitude > 0.5f) {
            prevPos = transform.position;
            lastMovement = Time.time;
        }

        if (!ReprojectionManager.worldBounds.Contains(transform.position)) gameObject.SetActive(false);
    }

    void AvoidCollisions () {
        Collider[] colliders = Physics.OverlapSphere(transform.position, 5, 1 << gameObject.layer);
        Vector3 v = Vector3.zero;
        for (int i = 0; i < colliders.Length; i++) {
            KidController kid = colliders[i].GetComponent<KidController>();
            if (kid != null && kid.id > this.id) {
                Vector3 diff = transform.position - kid.transform.position;
                v += diff;
                break;
            }
        }
        if (v.sqrMagnitude > 0.01f) targetVelocity = Vector3.Lerp(targetVelocity, v, 0.5f);
    }

    void UpdateHighLow () {
        int count = DepthSourceView.highPoints == null ? 0 : DepthSourceView.lowPoints.Length;
        if (count == 0 || id < 0) {
            highPoint = transform.position;
            lowPoint = transform.position;
        }
        else {
            int index = id % count;
            highPoint = DepthSourceView.highPoints[index];
            lowPoint = DepthSourceView.lowPoints[index];
        }
    }

    Vector3 lastKnownPosition;
    void BuryBody () {
        state = State.Burried;
        sled.SetActive(false);
        anim.SetBool("sledding", false);
        anim.SetBool("burried", true);
        lastKnownPosition = transform.position;
        body.isKinematic = true;
        PlayParticle("Exclamation", 5);
        transform.position -= Vector3.up;
        body.velocity = Vector3.zero;
        activeCount--;
        lastDespawnTime = Time.time;
    }

    void UpdateBurried () {
        Ray ray = new Ray(lastKnownPosition + Vector3.up * 0.05f, Vector3.down);
        RaycastHit hit;
        if (ReprojectionManager.depthMeshCollider.Raycast(ray, out hit, 1000)) {
            groundDist = hit.distance;
            groundNormal = hit.normal;
            state = State.Ragdoll;
            body.isKinematic = false;
            body.transform.position = lastKnownPosition+Vector3.up*buryDepth;
            body.velocity = Vector3.up * Mathf.Sqrt(-2 * Physics.gravity.y * buryDepth);
            PlayParticle("SnowPuff", 0);
            anim.SetBool("burried", false);
            activeCount++;
        }
    }

    void UpdateRagdoll () {
        targetVelocity = Vector3.zero;
        
        // if idle for long enough, find a high point, and climb
        if (Time.time - lastMovement < ragdollIdleTime + id) return;

        Vector3 diff = highPoint - lowPoint;
        if (diff.y > sledHeight && Random.value > 0.5f) {
            state = State.Climb;
            climbTarget = highPoint;
        }
        else {
            wanderStartTime = Time.time;
            state = State.Wander;
        }
    }

    float wanderStartTime;
    void UpdateWander () {
        if (targetVelocity.sqrMagnitude < 1) {
            targetVelocity = Random.insideUnitSphere;
            targetVelocity.y = 0;
            targetVelocity.Normalize();
            targetVelocity *= walkSpeed * Random.Range(0.5f, 1.1f);
        }

        targetVelocity = Quaternion.Euler(0, Random.Range(-15,15)*Time.deltaTime, 0) * targetVelocity;
        if (Random.value < 0.01f && Time.time - wanderStartTime > 2) state = State.Ragdoll;
    }

    void UpdateClimb () {
        // if diff between high and low is no longer big enough, give up
        Vector3 diff = highPoint - lowPoint;
        if (diff.y < sledHeight) {
            state = State.Ragdoll;
            return;
        }

        // if high point changes too much, pick another
        Ray ray = new Ray(climbTarget + Vector3.up * 10, Vector3.down);
        RaycastHit hit;
        if (!ReprojectionManager.depthMeshCollider.Raycast(ray, out hit, 1000) || hit.distance > 12) {
            climbTarget = highPoint;
        }

        diff = climbTarget - transform.position;
        diff.y = 0;
        float dist = diff.magnitude;
        targetVelocity = diff * walkSpeed / dist;

        // if high point reached, go downhill
        if (dist < 0.5f) StartSledding();
    }

    void StartSledding () {
        state = State.ReadySled;
        sled.SetActive(true);
        Vector3 dir = transform.forward;
        if (Random.value > 0.3f) dir = (lowPoint - highPoint).normalized;
        dir.y = 0;
        transform.forward = dir;
        transform.position += Vector3.up * 1;
        targetVelocity = Vector3.zero;
        body.velocity = Vector3.up + transform.forward*2;
        anim.SetBool("sledding", true);
        PlayParticle("Pop");
    }

    void UpdateReadySled () {
        if (groundDist > 1f) return;
        state = State.Sledding;
        targetVelocity = transform.forward * sledSpeed;
        body.velocity = targetVelocity;
    }

    void UpdateSledding () {
        Vector3 sledVelocity = groundNormal;
        sledVelocity.y = 0;
        sledVelocity = Vector3.Lerp(sledVelocity, sledVelocity.normalized, 0.9f) * sledSpeed;
        targetVelocity = Vector3.Lerp(body.velocity, sledVelocity, Time.deltaTime * 0.5f);

        // if stopped, ragdoll
        if (body.velocity.sqrMagnitude < 1f) {
            state = State.Ragdoll;
            PlayParticle("SnowPuff", 4);
            sled.SetActive(false);
            anim.SetBool("sledding", false);
        }
    }

    float groundDist;
    Vector3 groundNormal;
    void GroundCheck () {
        if (state == State.Burried) return;

        Ray ray = new Ray(transform.position + Vector3.up * buryDepth, Vector3.down);
        RaycastHit hit;
        if (ReprojectionManager.depthMeshCollider.Raycast(ray, out hit, 1000)) {
            groundDist = hit.distance - buryDepth;
            groundNormal = hit.normal;
            if (groundDist < 0) {
                transform.position -= Vector3.up*groundDist;
                groundDist=0;
            }
        }
        else {
            groundDist = -1;
            BuryBody();
        }
        anim.SetFloat("groundDist", groundDist);
    }

    void UpdateOrientation () {
        //orient to velocity
        Vector3 v = body.velocity;
        v.y = 0;
        if (v.sqrMagnitude > 0.1f) {
            float turn = Mathf.Sign(Vector3.Cross(transform.forward, v).y);
            float dot = Vector3.Dot(transform.forward, v);
            if (dot < -0.9f) transform.forward = v;
            else transform.Rotate(Vector3.up * turn * turnSpeed * Time.deltaTime);
        }
    }

    void FixedUpdate () {
        if (state != State.Ragdoll) {
            Vector3 velocityChange = targetVelocity - body.velocity;
            velocityChange.y = 0;
            float speedChange = velocityChange.magnitude;
            if (speedChange > maxSpeedChange) velocityChange = velocityChange * maxSpeedChange / speedChange;
            body.AddForce(velocityChange, ForceMode.VelocityChange);
        }

        Vector3 v = body.velocity;
        v.y = 0;
        float s = v.magnitude;
        anim.SetFloat("speed", s);
    }

    void PlayParticle (string name, float up = 1) {
        ParticleManager.Play(name, transform.position + Vector3.up * up, Vector3.up);
    }
}
