Shader "Snow Kids/Depth Map"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _DepthTex ("Depth", 2D) = "white" {}
        _GradientTex ("Gradient", 2D) = "white" {}
        _Blend ("Blend", Range(0,1)) = 0
        _Brightness ("Dim", Range(0,1)) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float2 depthUV : TEXCOORD1;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            sampler2D _DepthTex;
            float4 _DepthTex_ST;
            sampler2D _GradientTex;
            float _Blend;
            float _Brightness;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.depthUV = TRANSFORM_TEX(v.uv, _DepthTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float d = saturate(tex2D(_DepthTex, i.depthUV).r);
                fixed4 g = tex2D(_GradientTex, float2(d, 0));
                fixed4 c = tex2D(_MainTex, i.uv) * _Brightness;
                return lerp(c, g, _Blend);
            }
            ENDCG
        }
    }
}
