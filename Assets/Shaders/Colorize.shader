Shader "Snow Kids/Colorize"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;

        struct Input {
            float2 uv_MainTex;
        };

        half _Glossiness;
        half _Metallic;
        float4 _Color;

        static const float PI = 3.14159265359;
        static const float HALF_PI = PI * 0.5;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        float2 rgb_to_sat_val(float3 rgb) {
            float rgb_min = min(min(rgb.r, rgb.g), rgb.b);
            float rgb_max = max(max(rgb.r, rgb.g), rgb.b);
            return float2((rgb_max - rgb_min)/rgb_max, rgb_max);
        }

        float3 hsv_to_rgb(float3 hsv) {
            float3 rgb = hsv.zzz;

            float h = hsv.x * 6;
            int i = floor(h);
            float a = hsv.z * (1.0 - hsv.y);
            float b = hsv.z * (1.0 - hsv.y * (h-i));
            float c = hsv.z * (1.0 - hsv.y * (1-(h-i)));
            if (i == 0) { rgb = float3(hsv.z, c, a); }
            else if (i == 1) { rgb = float3(b, hsv.z, a); }
            else if (i == 2) { rgb = float3(a, hsv.z, c); }
            else if (i == 3) { rgb = float3(a, b, hsv.z); }
            else if (i == 4) { rgb = float3(c, a, hsv.z); }
            else { rgb = float3(hsv.z, a, b); }
        
            return rgb;
        }

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
            float2 sv = rgb_to_sat_val(c.rgb);
            float t = sv.x + sv.x * sin(sv.y * HALF_PI);
            c.rgb = lerp(c.rgb, _Color.rgb, t);

            o.Albedo = c.rgb;
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a * _Color.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
