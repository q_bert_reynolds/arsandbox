Shader "Snow Kids/Snowy Terrain"
{
    Properties
    {
        _MaskTex ("Texture", 2D) = "white" {}
        _DepthTex ("Depth", 2D) = "white" {}
        _LogoTex ("Logo", 2D) = "black" {}
        _LightsGradient ("Lights Gradient", 2D) = "white" {}
        _SnowGradient ("Snow Gradient", 2D) = "white" {}
        _NoiseTex ("Noise Texutre", 2D) = "black" {}
        _Flow ("Flow (X, Y, Cycle Time, Cycle Speed)", Vector) = (1, 1, 0.1, 1)
        _Noise ("Noise Strength", Range(0,1)) = 0.5
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MaskTex;
        sampler2D _DepthTex;
        sampler2D _LightsGradient;
        sampler2D _SnowGradient;
        sampler2D _NoiseTex;
        sampler2D _LogoTex;
        float4 _Flow;
        float _Noise;
        float3 _WorldOrigin;
        float3 _WorldSize;

        struct Input
        {
            float2 uv_MaskTex;
            float2 uv_DepthTex;
            float3 worldNormal;
            float3 worldPos;
        };

        void surf (Input i, inout SurfaceOutputStandard o) {
            float n = (2*tex2D(_NoiseTex, i.uv_DepthTex)-1).r * _Noise;
                float depth = saturate(tex2D(_DepthTex, i.uv_DepthTex).r);
                float d = saturate(depth*2-1);
                float4 g = tex2D(_LightsGradient, fmod(d-_Time.x, 1));
                float4 s = tex2D(_SnowGradient, depth);

                float2 flow = i.worldNormal.xz;
                flow.x *= _Flow.x;
                flow.y *= _Flow.y;

                float halfCycle = _Flow.z * 0.5f; 
                float t = (_Time.x + n) * _Flow.w;
				float t0 = fmod(t + halfCycle, _Flow.z);
				float t1 = fmod(t, _Flow.z);     
				float blend = abs(t0 - halfCycle) / halfCycle;

				float4 m1 = tex2D(_MaskTex, i.uv_MaskTex + flow * t0);
				float4 m2 = tex2D(_MaskTex, i.uv_MaskTex + flow * t1);
				float4 m = lerp(m1, m2, blend);

                float p = fmod(d+_Time.x/4+n.x,1)*4;
                float c = lerp(m.a, m.r, saturate(p-3));
                c = lerp(m.b, c, saturate(p-2));
                c = lerp(m.g, c, saturate(p-1));
                c = lerp(m.r, c, saturate(p));
                c = lerp(0, c, saturate(depth*9-5));
                
                float worldOffset = (_WorldSize.z-_WorldSize.x) * 0.5;
                float2 worldUV = saturate(((i.worldPos - _WorldOrigin + float3(worldOffset,0,0))/_WorldSize.z).xz);
                float4 logo = tex2D(_LogoTex, worldUV);

                o.Albedo = s*s*s;
                float em = logo.a*smoothstep(0.75,1,depth);
                o.Emission = lerp(g*c, logo.rgb, em);
        }
        ENDCG
    }
    FallBack "Diffuse"
}
